/*
 * Copyright (c) 1998-2015 Erez Zadok
 * Copyright (c) 2009	   Shrikar Archak
 * Copyright (c) 2003-2015 Stony Brook University
 * Copyright (c) 2003-2015 The Research Foundation of SUNY
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include "trfs.h"
#include <linux/module.h>

/*
 * There is no need to lock the trfs_super_info's rwsem as there is no
 * way anyone can have a reference to the superblock at this point in time.
 */
static int trfs_read_super(struct super_block *sb, void *raw_data, int silent)
{
	int err = 0;
	mm_segment_t old_fs;
	struct super_block *lower_sb;
	struct path lower_path;
	struct file *outfile_ptr = NULL;
	umode_t outfile_mode;
	struct trfs_mount_info *read_super_args =
	(struct trfs_mount_info *) raw_data;
	char *dev_name = read_super_args->device_name;
	char *output_path = read_super_args->output_path;
	struct inode *inode;

	outfile_ptr = filp_open(output_path, O_RDONLY, 0);
	if (!outfile_ptr || IS_ERR(outfile_ptr)) {
		outfile_mode = S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH;
	} else {
		outfile_mode = outfile_ptr->f_path.dentry->d_inode->i_mode;
		old_fs = get_fs();
		set_fs(KERNEL_DS);
		vfs_unlink(outfile_ptr->f_path.dentry->d_parent->d_inode,
		 outfile_ptr->f_path.dentry, NULL);
		set_fs(old_fs);
		filp_close(outfile_ptr, NULL);
	}
	outfile_ptr = open_outfile(output_path, outfile_mode);
	if (outfile_ptr == NULL) {
		printk(KERN_ALERT "outfile_ptr NULL");
		err = -ENOENT;
		goto out;
	}

	if (!dev_name) {
		printk(KERN_ERR
		       "trfs: read_super: missing dev_name argument\n");
		err = -EINVAL;
		goto out;
	}

	/* parse lower path */
	err = kern_path(dev_name, LOOKUP_FOLLOW | LOOKUP_DIRECTORY,
			&lower_path);
	if (err) {
		printk(KERN_ERR	"trfs: error accessing "
		       "lower directory '%s'\n", dev_name);
		goto out;
	}

	/* allocate superblock private data */
	sb->s_fs_info = kzalloc(sizeof(struct trfs_sb_info), GFP_KERNEL);
	if (!TRFS_SB(sb)) {
		printk(KERN_CRIT "trfs: read_super: out of memory\n");
		err = -ENOMEM;
		goto out_free;
	}
	/* set the lower superblock field of upper superblock */
	lower_sb = lower_path.dentry->d_sb;
	atomic_inc(&lower_sb->s_active);
	trfs_set_lower_super(sb, lower_sb, outfile_ptr, "0xffffffff\0", 0, 0);

	/* inherit maxbytes from lower file system */
	sb->s_maxbytes = lower_sb->s_maxbytes;

	/*
	 * Our c/m/atime granularity is 1 ns because we may stack on file
	 * systems whose granularity is as good.
	 */
	sb->s_time_gran = 1;

	sb->s_op = &trfs_sops;

	sb->s_export_op = &trfs_export_ops; /* adding NFS support */

	/* get a new inode and allocate our root dentry */
	inode = trfs_iget(sb, d_inode(lower_path.dentry));
	if (IS_ERR(inode)) {
		err = PTR_ERR(inode);
		goto out_sput;
	}
	sb->s_root = d_make_root(inode);
	if (!sb->s_root) {
		err = -ENOMEM;
		goto out_iput;
	}
	d_set_d_op(sb->s_root, &trfs_dops);

	/* link the upper and lower dentries */
	sb->s_root->d_fsdata = NULL;
	err = new_dentry_private_data(sb->s_root);
	if (err)
		goto out_freeroot;

	/* if get here: cannot have error */

	/* set the lower dentries for s_root */
	trfs_set_lower_path(sb->s_root, &lower_path);

	/*
	 * No need to call interpose because we already have a positive
	 * dentry, which was instantiated by d_make_root.  Just need to
	 * d_rehash it.
	 */
	d_rehash(sb->s_root);
	if (!silent)
		printk(KERN_INFO
		       "trfs: mounted on top of %s type %s\n",
		       dev_name, lower_sb->s_type->name);
	goto out; /* all is well */

	/* no longer needed: free_dentry_private_data(sb->s_root); */
out_freeroot:
	dput(sb->s_root);
out_iput:
	iput(inode);
out_sput:
	/* drop refs we took earlier */
	atomic_dec(&lower_sb->s_active);
	kfree(TRFS_SB(sb));
	sb->s_fs_info = NULL;
out_free:
	path_put(&lower_path);

out:
	return err;
}

int get_out_file_path(void *raw_data, char **out_path)
{
	int retval  = 0;
	char *mount_option = (char *) raw_data;
	char *out_attrib = NULL;

	if (mount_option == NULL) {
		printk(KERN_ALERT "Mount option parameter is mandatory!");
		retval = -EINVAL;
		goto out;
	}
	out_attrib = strsep(&mount_option, "=");
	if (strcmp(out_attrib, "tfile") == 0) {
		if (mount_option == NULL) {
			printk(KERN_ALERT "Empty file path passed for output!");
			retval = -EINVAL;
			goto out;
		}
	} else {
		printk(KERN_ALERT "Mount option attribute should be 'tfile'!");
		retval = -EINVAL;
		goto out;
	}
	*out_path = kzalloc(strlen(mount_option), GFP_KERNEL);
	if (*out_path == NULL) {
		printk(KERN_ALERT "Unable to allocate memory to copy output file name!");
		retval = -ENOMEM;
		goto out;
	}
	strcpy(*out_path, mount_option);
out:
	return retval;
}

struct dentry *trfs_mount(struct file_system_type *fs_type, int flags,
			    const char *dev_name, void *raw_data)
{
	int retval = 0;
	struct trfs_mount_info *read_super_args = NULL;
	char *out_path = NULL;
	retval = get_out_file_path(raw_data, &out_path);

	printk(KERN_ALERT "========================Session======================");
	if (retval != 0) {
		printk(KERN_ALERT "Output file name extraction failed!");
		goto out;
	}
	printk(KERN_ALERT "Output file path received as: %s", out_path);
	read_super_args = kzalloc(sizeof(struct trfs_mount_info), GFP_KERNEL);
	if (read_super_args == NULL) {
		printk(KERN_ALERT "Unable to allocate memory to super block arg struct!");
		retval = -ENOMEM;
		goto out;
	}
	read_super_args->device_name = (char *) dev_name;
	read_super_args->output_path = out_path;
	return mount_nodev(fs_type, flags, (void *)read_super_args,
			   trfs_read_super);
out:
	if (read_super_args != NULL)
		kfree(read_super_args);
	if (out_path != NULL)
		kfree(out_path);
	return ERR_PTR(retval);
}

static struct file_system_type trfs_fs_type = {
	.owner		= THIS_MODULE,
	.name		= TRFS_NAME,
	.mount		= trfs_mount,
	.kill_sb	= generic_shutdown_super,
	.fs_flags	= 0,
};
MODULE_ALIAS_FS(TRFS_NAME);

static int __init init_trfs_fs(void)
{
	int err;

	pr_info("Registering trfs " TRFS_VERSION "\n");

	err = trfs_init_inode_cache();
	if (err)
		goto out;
	err = trfs_init_dentry_cache();
	if (err)
		goto out;
	err = register_filesystem(&trfs_fs_type);
out:
	if (err) {
		trfs_destroy_inode_cache();
		trfs_destroy_dentry_cache();
	}
	return err;
}

static void __exit exit_trfs_fs(void)
{
	trfs_destroy_inode_cache();
	trfs_destroy_dentry_cache();
	unregister_filesystem(&trfs_fs_type);
	pr_info("Completed trfs module unload\n");
}

MODULE_AUTHOR("Erez Zadok, Filesystems and Storage Lab, Stony Brook University"
	      " (http://www.fsl.cs.sunysb.edu/)");
MODULE_DESCRIPTION("trfs " TRFS_VERSION
		   " (http://trfs.filesystems.org/)");
MODULE_LICENSE("GPL");

module_init(init_trfs_fs);
module_exit(exit_trfs_fs);
