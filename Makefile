TRFS_VERSION="0.1"

EXTRA_CFLAGS += -DTRFS_VERSION=\"$(TRFS_VERSION)\"

obj-$(CONFIG_TR_FS) += trfs.o

trfs-y := dentry.o file.o inode.o main.o super.o lookup.o mmap.o

INC=/lib/modules/$(shell uname -r)/build/arch/x86/include

all:
	gcc -Wall -Werror -I$(INC)/generated/uapi -I$(INC)/uapi ../../hw2/trctl.c -o ../../hw2/trctl
	gcc -Wall -Werror -I$(INC)/generated/uapi -I$(INC)/uapi ../../hw2/treplay.c -o ../../hw2/treplay
