#!/bin/sh
set -x
# WARNING: this script doesn't check for errors,
# so you have to enhance it in case any of the commands
# below fail.
current_dir=$(pwd)
treplay_dir="/trfs_replay"
script_dir=$(dirname $0)
cd $script_dir/../fs/trfs
make
umount /mnt/trfs
rmmod trfs
lsmod
insmod trfs.ko
rm -rf $treplay_dir
mkdir -p $1 $treplay_dir
yes|cp -arf $1/* $treplay_dir
mount -t trfs -o tfile=$2 $1 /mnt/trfs/
$script_dir/trctl all /mnt/trfs/
