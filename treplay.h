#ifndef _TREPLAY_H_
#define _TREPLAY_H_

#define MAGIC_NUMBER 'M'
#define DISPLAY_BIT_MAP_IOCTL _IOR(MAGIC_NUMBER, 0, int)
#define SET_BIT_MAP_IOCTL _IOW(MAGIC_NUMBER, 1, int)
#define SET_ALL_BIT_MAP_IOCTL _IOW(MAGIC_NUMBER, 2, int)
#define SET_NONE_BIT_MAP_IOCTL _IOW(MAGIC_NUMBER, 3, int)

#define TRFS_CREATE_BIT_ID		1
#define TRFS_OPEN_BIT_ID		2
#define TRFS_READ_BIT_ID		4
#define TRFS_WRITE_BIT_ID		8
#define TRFS_MKDIR_BIT_ID		16
#define TRFS_RMDIR_BIT_ID		32
#define TRFS_SYMLINK_BIT_ID		64
#define TRFS_CLOSE_BIT_ID		128
#define TRFS_RENAME_BIT_ID		256
#define TRFS_UNLINK_BIT_ID		512
#define TRFS_LINK_BIT_ID        1024
#define TRFS_READLINK_BIT_ID    2048
#define TRFS_FSYNC_BIT_ID		4096

struct file_ops_record {
	unsigned int rec_sz;
	unsigned int rec_id;
	unsigned int rec_type;
	unsigned int flags;
	unsigned short file_mode;
	unsigned short path_len;
	unsigned short file_content_sz;
	long long offset;
	int retval;
	unsigned int stream_len;
	char stream[1];
};

#endif
