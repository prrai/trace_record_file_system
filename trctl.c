#ifdef EXTRA_CREDIT
#include "trctl.h"
#include "trfs_user.h"
#include "treplay.h"

long string_to_int(char *bit_map)
{
	unsigned long long_bit_map = 0;
	char *iter = bit_map + 2;
	bool invalid_string = false;

	while (*iter != '\0') {
		if (*iter >= '0' && *iter <= '9')
			long_bit_map = (long_bit_map << 4) +
			 (*iter - '0'); else
			if (*iter >= 'A' && *iter <= 'F')
			long_bit_map = (long_bit_map << 4) + (*iter - 'A' +
			 10); else
			if (*iter >= 'a' && *iter <= 'f')
			long_bit_map = (long_bit_map << 4) + (*iter - 'a' +
			 10); else {
			invalid_string = true;
			break;
		}
		iter++;
	}
	if (invalid_string)
		return -1;
	return long_bit_map;
}

int main(int argc, char *argv[])
{
	char cmd[15];
	char cmd2[15];
	char mount_path[256];
	char *tags[32];
	int tag_cnt = 0;
	int fd = 0, retval = 0;
	long bit_map = 0;

	if (argc < 2) {
		fprintf(stderr, "Mount path argument can not be empty!\n");
		return -1;
	}
	memset(&mount_path, 0, sizeof(mount_path));
	memset(&cmd, 0, sizeof(cmd));
	if (argc == 2) {
		strcpy(cmd, "");
		cmd[strlen(argv[optind])] = '\0';
		strcpy(mount_path, argv[optind]);
		mount_path[strlen(argv[optind])] = '\0';
	} else {
		strcpy(mount_path, argv[argc-optind]);
		mount_path[strlen(argv[argc-optind])] = '\0';
		for (tag_cnt = 0; tag_cnt < argc-optind-1; tag_cnt++) {
			tags[tag_cnt] = argv[tag_cnt + optind];
			printf("%s\n", tags[tag_cnt]);
		}
		printf("mount path: %s\n", mount_path);
	}
	fd = open(mount_path, O_RDONLY);
	if (fd < 0) {
		perror("Unable to attach file descriptor to ioctl\n");
		return -1;
	}
	retval = ioctl(fd, DISPLAY_BIT_MAP_IOCTL, cmd);
	if (retval != 0) {
		perror("Error ocurred on trying to retrieve bit map\n");
		return -1;
	}
	printf("The current value of bit map is: %s\n", cmd);
	if (argc > 2) {
		printf("Apply system call tags from commandline..\n");
		bit_map = string_to_int(cmd);
		printf("Bit map(int) before processing tags: %ld\n",
		 bit_map);
		for (tag_cnt = 0; tag_cnt < argc-optind-1; tag_cnt++) {
			printf("*****************\n");
			printf("Current system call tag: %s\n",
			 tags[tag_cnt]);
			printf("Check if system call implemented..\n");
			if (strstr(tags[tag_cnt], "create") ||
			 strstr(tags[tag_cnt], "CREATE")) {
				printf("Nearest match: %s, process..\n",
				 "create");
				if ((bit_map & TRFS_CREATE_BIT_ID) &&
				 (tags[tag_cnt][0] == '-'))
					bit_map -= TRFS_CREATE_BIT_ID;
				if (!(bit_map & TRFS_CREATE_BIT_ID) &&
				 (tags[tag_cnt][0] == '+'))
					bit_map += TRFS_CREATE_BIT_ID;
			} else if (strstr(tags[tag_cnt], "open") ||
			 strstr(tags[tag_cnt], "OPEN")) {
				printf("Nearest match: %s, process\n",
				 "open");
				if ((bit_map & TRFS_OPEN_BIT_ID) &&
				 (tags[tag_cnt][0] == '-'))
					bit_map -= TRFS_OPEN_BIT_ID;
				if (!(bit_map & TRFS_OPEN_BIT_ID) &&
				 (tags[tag_cnt][0] == '+'))
					bit_map += TRFS_OPEN_BIT_ID;
			} else if (strstr(tags[tag_cnt], "close") ||
			 strstr(tags[tag_cnt], "CLOSE")) {
				printf("Nearest match: %s, process\n",
				 "close");
				if ((bit_map & TRFS_CLOSE_BIT_ID) &&
				 (tags[tag_cnt][0] == '-'))
					bit_map -= TRFS_CLOSE_BIT_ID;
				if (!(bit_map & TRFS_CLOSE_BIT_ID) &&
				 (tags[tag_cnt][0] == '+'))
					bit_map += TRFS_CLOSE_BIT_ID;
			} else if (strstr(tags[tag_cnt], "readlink") ||
			 strstr(tags[tag_cnt], "READLINK")) {
				printf("Nearest match: %s, process\n",
				 "readlink");
				if ((bit_map & TRFS_READLINK_BIT_ID) &&
				 (tags[tag_cnt][0] == '-'))
					bit_map -= TRFS_READLINK_BIT_ID;
				if (!(bit_map & TRFS_READLINK_BIT_ID) &&
				 (tags[tag_cnt][0] == '+'))
					bit_map += TRFS_READLINK_BIT_ID;
			} else if (strstr(tags[tag_cnt], "symlink") ||
			 strstr(tags[tag_cnt], "SYMLINK")) {
				printf("Nearest match: %s, process\n",
				 "symlink");
				if ((bit_map & TRFS_SYMLINK_BIT_ID) &&
				 (tags[tag_cnt][0] == '-'))
					bit_map -= TRFS_SYMLINK_BIT_ID;
				if (!(bit_map & TRFS_SYMLINK_BIT_ID) &&
				 (tags[tag_cnt][0] == '+'))
					bit_map += TRFS_SYMLINK_BIT_ID;
			} else if (strstr(tags[tag_cnt], "unlink") ||
			 strstr(tags[tag_cnt], "UNLINK")) {
				printf("Nearest match: %s, process\n",
				 "unlink");
				if ((bit_map & TRFS_UNLINK_BIT_ID) &&
				 (tags[tag_cnt][0] == '-'))
					bit_map -= TRFS_UNLINK_BIT_ID;
				if (!(bit_map & TRFS_UNLINK_BIT_ID) &&
				 (tags[tag_cnt][0] == '+'))
					bit_map += TRFS_UNLINK_BIT_ID;
			} else if (strstr(tags[tag_cnt], "read") ||
			 strstr(tags[tag_cnt], "READ")) {
				printf("Nearest match: %s, process\n",
				 "read");
				if ((bit_map & TRFS_READ_BIT_ID) &&
				 (tags[tag_cnt][0] == '-'))
					bit_map -= TRFS_READ_BIT_ID;
				if (!(bit_map & TRFS_READ_BIT_ID) &&
				 (tags[tag_cnt][0] == '+'))
					bit_map += TRFS_READ_BIT_ID;
			} else if (strstr(tags[tag_cnt], "link") ||
			 strstr(tags[tag_cnt], "LINK")) {
				printf("Nearest match: %s, process\n",
				 "link");
				if ((bit_map & TRFS_READLINK_BIT_ID) &&
				 (tags[tag_cnt][0] == '-'))
					bit_map -= TRFS_READLINK_BIT_ID;
				if (!(bit_map & TRFS_READLINK_BIT_ID) &&
				 (tags[tag_cnt][0] == '+'))
					bit_map += TRFS_READLINK_BIT_ID;
			} else if (strstr(tags[tag_cnt], "write") ||
			 strstr(tags[tag_cnt], "WRITE")) {
				printf("Nearest match: %s, process\n",
				 "write");
				if ((bit_map & TRFS_WRITE_BIT_ID) &&
				 (tags[tag_cnt][0] == '-'))
					bit_map -= TRFS_WRITE_BIT_ID;
				if (!(bit_map & TRFS_WRITE_BIT_ID) &&
				 (tags[tag_cnt][0] == '+'))
					bit_map += TRFS_WRITE_BIT_ID;
			} else if (strstr(tags[tag_cnt], "mkdir") ||
			 strstr(tags[tag_cnt], "MKDIR")) {
				printf("Nearest match: %s, process\n",
				 "mkdir");
				if ((bit_map & TRFS_MKDIR_BIT_ID) &&
				 (tags[tag_cnt][0] == '-'))
					bit_map -= TRFS_MKDIR_BIT_ID;
				if (!(bit_map & TRFS_MKDIR_BIT_ID) &&
				 (tags[tag_cnt][0] == '+'))
					bit_map += TRFS_MKDIR_BIT_ID;
			} else if (strstr(tags[tag_cnt], "rmdir") ||
			 strstr(tags[tag_cnt], "RMDIR")) {
				printf("Nearest match: %s, process\n",
				 "rmdir");
				if ((bit_map & TRFS_RMDIR_BIT_ID) &&
				 (tags[tag_cnt][0] == '-'))
					bit_map -= TRFS_RMDIR_BIT_ID;
				if (!(bit_map & TRFS_RMDIR_BIT_ID) &&
				 (tags[tag_cnt][0] == '+'))
					bit_map += TRFS_RMDIR_BIT_ID;
			} else if (strstr(tags[tag_cnt], "rename") ||
			 strstr(tags[tag_cnt], "RENAME")) {
				printf("Nearest match: %s, process\n",
				 "rename");
				if ((bit_map & TRFS_RENAME_BIT_ID) &&
				 (tags[tag_cnt][0] == '-'))
					bit_map -= TRFS_RENAME_BIT_ID;
				if (!(bit_map & TRFS_RENAME_BIT_ID) &&
				 (tags[tag_cnt][0] == '+'))
					bit_map += TRFS_RENAME_BIT_ID;
			} else if (strstr(tags[tag_cnt], "fsync") ||
			 strstr(tags[tag_cnt], "FSYNC")) {
				printf("Nearest match: %s, process\n",
				 "fsync");
				if ((bit_map & TRFS_FSYNC_BIT_ID) &&
				 (tags[tag_cnt][0] == '-'))
					bit_map -= TRFS_FSYNC_BIT_ID;
				if (!(bit_map & TRFS_FSYNC_BIT_ID) &&
				 (tags[tag_cnt][0] == '+'))
					bit_map += TRFS_FSYNC_BIT_ID;
			} else if (strstr(tags[tag_cnt], "all") ||
			 strstr(tags[tag_cnt], "ALL")) {
				printf("Set all..\n");
				bit_map = 4294967295;
			} else if (strstr(tags[tag_cnt], "none") ||
			 strstr(tags[tag_cnt], "NONE")) {
				printf("Set none..\n");
				bit_map = 0;
			} else {
				fprintf(stderr,
				 "System call tag: %s is invalid!\n",
				 tags[tag_cnt]);
				return -1;
			}
			printf("*****************\n");
		}
		printf("Bit map(int) value after  tags: %ld\n",
		 bit_map);
		memset(cmd, 0, strlen(cmd));
		memcpy(cmd, "0x", 2);
		sprintf(cmd2, "%lx", bit_map);
		memcpy(cmd + 2, cmd2, strlen(cmd2));
		printf("Final bit map after processing tags: %s\n",
		 cmd);
		retval = ioctl(fd, SET_BIT_MAP_IOCTL, cmd);
		if (retval != 0) {
			if (errno == 22) {
				perror("Final hexadecimal invalid!\n");
				return -1;
			}
			perror("Error occured on modifying bit map\n");
			return -1;
		}
		printf("Bit map was successfully set to: %s\n",
		 cmd);
	}
	return 0;
}

#else

#include "trctl.h"
#include "trfs_user.h"

int main(int argc, char *argv[])
{
	char cmd[256];
	char mount_path[256];
	int fd = 0, retval = 0;

	if (argc < 2) {
		fprintf(stderr, "Mount path argument can not be empty!\n");
		return -1;
	}
	memset(&mount_path, 0, sizeof(mount_path));
	memset(&cmd, 0, sizeof(cmd));
	if (argc == 2) {
		strcpy(cmd, "");
		cmd[strlen(argv[optind])] = '\0';
		strcpy(mount_path, argv[optind]);
		mount_path[strlen(argv[optind])] = '\0';
	} else if (argc == 3) {
		strcpy(cmd, argv[optind]);
		cmd[strlen(argv[optind])] = '\0';
		strcpy(mount_path, argv[optind + 1]);
		mount_path[strlen(argv[optind + 1])] = '\0';
	} else {
		fprintf(stderr,
		 "Too many arguments in the command line!\n");
		return -1;
	}
	fd = open(mount_path, O_RDONLY);
	if (fd < 0) {
		perror("Unable to attach file descriptor to ioctl\n");
		return -1;
	}
	if (strcmp(cmd, "\0") == 0) {
		retval = ioctl(fd, DISPLAY_BIT_MAP_IOCTL, cmd);
		if (retval != 0) {
			perror("Error occured while display bit map\n");
			return -1;
		}
		printf("Current value of bit map is: %s\n", cmd);
	} else {
		if (strcmp(cmd, "all") == 0 || strcmp(cmd, "ALL") == 0)
			retval = ioctl(fd,
			 SET_ALL_BIT_MAP_IOCTL, cmd); else
			if (strcmp(cmd, "none") == 0 ||
			 strcmp(cmd, "NONE") == 0)
				retval = ioctl(fd,
				 SET_NONE_BIT_MAP_IOCTL, cmd); else
				retval = ioctl(fd,
				 SET_BIT_MAP_IOCTL, cmd);
		if (retval != 0) {
			if (errno == 22) {
				perror("Input hexadecimal is invalid!");
				return -1;
			}
			perror("Error occured on modifying bit map\n");
			return -1;
		}
		printf("Bit map was successfully set to: %s\n", cmd);
	}
	return 0;
}

#endif
