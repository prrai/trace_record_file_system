/*
 * Copyright (c) 1998-2015 Erez Zadok
 * Copyright (c) 2009	   Shrikar Archak
 * Copyright (c) 2003-2015 Stony Brook University
 * Copyright (c) 2003-2015 The Research Foundation of SUNY
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include "trfs.h"

static ssize_t trfs_read(struct file *file, char __user *buf,
			   size_t count, loff_t *ppos)
{
	int err;
	int record_total_length = 0;
	void *record = NULL;
	char *path = NULL;
	char *stream = NULL;
	int fill_path_len = 256;
	struct file *lower_file;
	char *kern_buf = NULL;
	struct dentry *dentry = file->f_path.dentry;
	struct file_ops_record *trfs_read = kzalloc(sizeof(struct file_ops_record),
	 GFP_KERNEL);
	char *fill_path = kzalloc(256, GFP_KERNEL);

	if (trfs_read == NULL) {
		err = -ENOMEM;
		goto out_err;
	}
	if (fill_path == NULL) {
		err = -ENOMEM;
		goto out_err;
	}
	lower_file = trfs_lower_file(file);
	err = vfs_read(lower_file, buf, count, ppos);

	/* update our inode atime upon a successful lower read */
	if (err >= 0)
		fsstack_copy_attr_atime(d_inode(dentry),
					file_inode(lower_file));

	if (TRFS_SB(file->f_path.dentry->d_inode->i_sb)->long_bit_map & TRFS_READ_BIT_ID) {
		printk(KERN_ALERT "Tracing trfs_read()");
		trfs_read->rec_type = TRFS_READ_BIT_ID;
		path = dentry_path_raw(file->f_path.dentry, fill_path, fill_path_len);
		trfs_read->path_len = strlen(path);
		trfs_read->flags = lower_file->f_flags;
		trfs_read->file_mode = lower_file->f_path.dentry->d_inode->i_mode;
		trfs_read->offset = *ppos - err;
		trfs_read->file_content_sz = err;
		printk(KERN_ALERT "[count] %d, [err] %d, [llpos] %d, [buf] %s", (int)count, err, (int)*ppos, buf);
		if (err > 0) {
			kern_buf = kzalloc(err, GFP_KERNEL);
			if (kern_buf == NULL) {
				err = -EFAULT;
				goto out_err;
			}
			copy_from_user(kern_buf, buf, err);
		}
		trfs_read->retval = err;
		stream = kzalloc(trfs_read->path_len + trfs_read->file_content_sz, GFP_KERNEL);
		if (stream == NULL) {
			err = -ENOMEM;
			goto out_err;
		}
		strncat(stream, path, trfs_read->path_len);
		if (err > 0)
			strncat(stream, kern_buf, trfs_read->file_content_sz);
		record = serialize_file_ops_struct(trfs_read, stream, &record_total_length);
		if (record == NULL) {
			printk(KERN_ALERT "Null buf returned");
			err = -EFAULT;
			goto out_err;
		}
		write_file(TRFS_SB(file->f_path.dentry->d_inode->i_sb)->outfile_ptr, record, record_total_length, TRFS_SB(file->f_path.dentry->d_inode->i_sb));
	}
	out_err:
		if (fill_path != NULL)
			kfree(fill_path);
		if (trfs_read != NULL)
			kfree(trfs_read);
		if (kern_buf != NULL)
			kfree(kern_buf);
		if (stream != NULL)
			kfree(stream);
		if (record != NULL)
			kfree(record);
		return err;
}

static ssize_t trfs_write(struct file *file, const char __user *buf,
			    size_t count, loff_t *ppos)
{
	int err;
	void *record = NULL;
	int record_total_length = 0;
	char *path = NULL;
	char *kern_buf = NULL;
	char *stream = NULL;
	int fill_path_len = 256;
	struct file *lower_file;
	struct dentry *dentry = file->f_path.dentry;
	struct file_ops_record *trfs_write = kzalloc(sizeof(struct file_ops_record), GFP_KERNEL);
	char *fill_path = kzalloc(256, GFP_KERNEL);
	if (!trfs_write) {
		err = -ENOMEM;
		goto out_err;
	}
	if (fill_path == NULL) {
		err = -ENOMEM;
		goto out_err;
	}
	lower_file = trfs_lower_file(file);
	err = vfs_write(lower_file, buf, count, ppos);

	/* update our inode times+sizes upon a successful lower write */
	if (err >= 0) {
		fsstack_copy_inode_size(d_inode(dentry),
					file_inode(lower_file));
		fsstack_copy_attr_times(d_inode(dentry),
					file_inode(lower_file));
	}
	if (TRFS_SB(file->f_path.dentry->d_inode->i_sb)->long_bit_map & TRFS_WRITE_BIT_ID) {
		printk(KERN_ALERT "Tracing trfs_write()");
		trfs_write->rec_type = TRFS_WRITE_BIT_ID;
		path = dentry_path_raw(file->f_path.dentry, fill_path, fill_path_len);
		trfs_write->path_len = strlen(path);
		trfs_write->flags = file->f_flags;
		trfs_write->file_mode = file->f_inode->i_mode;
		trfs_write->offset = *ppos;
		printk(KERN_ALERT "[count] %d, [err] %d, [llpos] %d, [buf] %s, [strlen buf] %d", (int)count, err, (int)*ppos, buf, (int)strlen_user(buf));
		trfs_write->file_content_sz = err;
		if (err > 0) {
			kern_buf = kmalloc(trfs_write->file_content_sz, GFP_KERNEL);
			if (kern_buf == NULL) {
				err = -EFAULT;
				goto out_err;
			}
			copy_from_user(kern_buf, buf, err);
		}
		trfs_write->retval = err;
		stream = kzalloc(trfs_write->path_len + trfs_write->file_content_sz, GFP_KERNEL);
		if (stream == NULL) {
			err = -ENOMEM;
			goto out_err;
		}
		strncat(stream, path, trfs_write->path_len);
		if (err > 0)
			strncat(stream, buf, trfs_write->file_content_sz);
		record = serialize_file_ops_struct(trfs_write, stream, &record_total_length);
		if (record == NULL) {
			printk(KERN_ALERT "Null buf returned");
			err = -EFAULT;
			goto out_err;
		}
		write_file(TRFS_SB(file->f_path.dentry->d_inode->i_sb)->outfile_ptr,
			record, record_total_length,
			TRFS_SB(file->f_path.dentry->d_inode->i_sb));
	}
	out_err:
		if (fill_path != NULL)
			kfree(fill_path);
		if (trfs_write != NULL)
			kfree(trfs_write);
		if (record != NULL)
			kfree(record);
		if (stream != NULL)
			kfree(stream);
		if (kern_buf != NULL)
			kfree(kern_buf);
		return err;
}

static int trfs_readdir(struct file *file, struct dir_context *ctx)
{
	int err;
	struct file *lower_file = NULL;
	struct dentry *dentry = file->f_path.dentry;
	lower_file = trfs_lower_file(file);
	err = iterate_dir(lower_file, ctx);
	file->f_pos = lower_file->f_pos;
	if (err >= 0)		/* copy the atime */
		fsstack_copy_attr_atime(d_inode(dentry),
					file_inode(lower_file));
	return err;
}

static long trfs_unlocked_ioctl(struct file *file, unsigned int cmd,
				  unsigned long arg)
{
	char *bit_map = NULL;
	int retval = 0, set_str = 0;
	switch (cmd) {
	case DISPLAY_BIT_MAP_IOCTL:
		printk(KERN_ALERT "The current bit map value is: %s", TRFS_SB((file->f_path.dentry->d_inode->i_sb))->bit_map);
		break;

	case SET_BIT_MAP_IOCTL:
		bit_map = kzalloc(strlen_user((char *) arg), GFP_KERNEL);
		if (bit_map == NULL) {
			printk(KERN_ALERT "Unable to allocate memory to bit map!");
			retval = -ENOMEM;
			goto out;
		}
		retval = strncpy_from_user(bit_map, (char *) arg, strlen_user((char *) arg));
		if (retval < 0) {
			printk(KERN_ALERT "bit map copy from user to kernel space failed!");
			goto out;
		}
		TRFS_SB((file->f_path.dentry->d_inode->i_sb))->bit_map = bit_map;
		retval = set_long_bit_map(file, bit_map);
		if (retval != 0) {
			retval = -EINVAL;
			printk(KERN_ALERT "Invalid hexadecimal number in the argument");
			goto out;
		}
		break;
	case SET_ALL_BIT_MAP_IOCTL:
		set_str = strlen("0xffffffff\0");
		bit_map = kzalloc(set_str, GFP_KERNEL);
		if (bit_map == NULL) {
			printk(KERN_ALERT "Unable to allocate memory to bit map!");
			retval = -ENOMEM;
			goto out;
		}
		strcpy(bit_map, "0xffffffff\0");
		TRFS_SB((file->f_path.dentry->d_inode->i_sb))->bit_map = bit_map;
		TRFS_SB((file->f_path.dentry->d_inode->i_sb))->long_bit_map = 4294967295;
		break;
	case SET_NONE_BIT_MAP_IOCTL:
		set_str = strlen("0x00000000\0");
		bit_map = kzalloc(set_str, GFP_KERNEL);
		if (bit_map == NULL) {
			printk(KERN_ALERT "Unable to allocate memory to bit map!");
			retval = -ENOMEM;
			goto out;
		}
		strcpy(bit_map, "0x00000000\0");
		TRFS_SB((file->f_path.dentry->d_inode->i_sb))->bit_map = bit_map;
		TRFS_SB((file->f_path.dentry->d_inode->i_sb))->long_bit_map = 0;
		break;
	}
	copy_to_user((void *) arg, (void *) TRFS_SB((file->f_path.dentry->d_inode->i_sb))->bit_map, strlen(TRFS_SB((file->f_path.dentry->d_inode->i_sb))->bit_map));
	retval = 0;
out:
	return retval;
}

#ifdef CONFIG_COMPAT
static long trfs_compat_ioctl(struct file *file, unsigned int cmd,
				unsigned long arg)
{
	long err = -ENOTTY;
	struct file *lower_file;

	lower_file = trfs_lower_file(file);

	/* XXX: use vfs_ioctl if/when VFS exports it */
	if (!lower_file || !lower_file->f_op)
		goto out;
	if (lower_file->f_op->compat_ioctl)
		err = lower_file->f_op->compat_ioctl(lower_file, cmd, arg);
out:
	return err;
}
#endif

static int trfs_mmap(struct file *file, struct vm_area_struct *vma)
{
	int err = 0;
	bool willwrite;
	struct file *lower_file;
	const struct vm_operations_struct *saved_vm_ops = NULL;
	/* this might be deferred to mmap's writepage */
	willwrite = ((vma->vm_flags | VM_SHARED | VM_WRITE) == vma->vm_flags);

	/*
	 * File systems which do not implement ->writepage may use
	 * generic_file_readonly_mmap as their ->mmap op.  If you call
	 * generic_file_readonly_mmap with VM_WRITE, you'd get an -EINVAL.
	 * But we cannot call the lower ->mmap op, so we can't tell that
	 * writeable mappings won't work.  Therefore, our only choice is to
	 * check if the lower file system supports the ->writepage, and if
	 * not, return EINVAL (the same error that
	 * generic_file_readonly_mmap returns in that case).
	 */
	lower_file = trfs_lower_file(file);
	if (willwrite && !lower_file->f_mapping->a_ops->writepage) {
		err = -EINVAL;
		printk(KERN_ERR "trfs: lower file system does not "
		       "support writeable mmap\n");
		goto out;
	}

	/*
	 * find and save lower vm_ops.
	 *
	 * XXX: the VFS should have a cleaner way of finding the lower vm_ops
	 */
	if (!TRFS_F(file)->lower_vm_ops) {
		err = lower_file->f_op->mmap(lower_file, vma);
		if (err) {
			printk(KERN_ERR "trfs: lower mmap failed %d\n", err);
			goto out;
		}
		saved_vm_ops = vma->vm_ops; /* save: came from lower ->mmap */
	}

	/*
	 * Next 3 lines are all I need from generic_file_mmap.  I definitely
	 * don't want its test for ->readpage which returns -ENOEXEC.
	 */
	file_accessed(file);
	vma->vm_ops = &trfs_vm_ops;

	file->f_mapping->a_ops = &trfs_aops; /* set our aops */
	if (!TRFS_F(file)->lower_vm_ops) /* save for our ->fault */
		TRFS_F(file)->lower_vm_ops = saved_vm_ops;

out:
	return err;
}

static int trfs_open(struct inode *inode, struct file *file)
{
	int err = 0;
	int rec_len = 0;
	char *path = NULL;
	int fill_path_len = 256;
	struct file *lower_file = NULL;
	struct path lower_path;
	struct file_ops_record *record = NULL;
	struct file_ops_record *trfs_open = kzalloc(sizeof(struct file_ops_record), GFP_KERNEL);
	char *stream = NULL;
	char *fill_path = kzalloc(256, GFP_KERNEL);

	if (!trfs_open) {
		err = -ENOMEM;
		goto out_err;
	}
	if (fill_path == NULL) {
		err = -ENOMEM;
		goto out_err;
	}
	/* don't open unhashed/deleted files */
	if (d_unhashed(file->f_path.dentry)) {
		err = -ENOENT;
		goto out_err;
	}

	file->private_data =
		kzalloc(sizeof(struct trfs_file_info), GFP_KERNEL);
	if (!TRFS_F(file)) {
		err = -ENOMEM;
		goto out_err;
	}

	/* open lower object and link trfs's file struct to lower's */
	trfs_get_lower_path(file->f_path.dentry, &lower_path);
	lower_file = dentry_open(&lower_path, file->f_flags, current_cred());
	path_put(&lower_path);
	if (IS_ERR(lower_file)) {
		err = PTR_ERR(lower_file);
		lower_file = trfs_lower_file(file);
		if (lower_file) {
			trfs_set_lower_file(file, NULL);
			fput(lower_file); /* fput calls dput for lower_dentry */
		}
	} else {
		trfs_set_lower_file(file, lower_file);
	}
	if (TRFS_SB(file->f_path.dentry->d_inode->i_sb)->long_bit_map & TRFS_OPEN_BIT_ID) {
		printk(KERN_ALERT "Tracing trfs_open()");
		trfs_open->rec_type = TRFS_OPEN_BIT_ID;
		path = dentry_path_raw(file->f_path.dentry, fill_path, fill_path_len);
		trfs_open->path_len = strlen(path);
		trfs_open->flags = lower_file->f_flags;
		trfs_open->file_mode = lower_file->f_path.dentry->d_inode->i_mode;
		trfs_open->file_content_sz = 0;
		trfs_open->retval = err;
		stream = kzalloc(trfs_open->path_len, GFP_KERNEL);
		if (stream == NULL) {
			err = -ENOMEM;
			goto out_err;
		}
		strncat(stream, path, trfs_open->path_len);
		record = serialize_file_ops_struct(trfs_open, stream, &rec_len);
		if (record == NULL) {
			printk(KERN_ALERT "Null buf returned");
			err = -ENOMEM;
			goto out_err;
		}
		write_file(TRFS_SB(file->f_path.dentry->d_inode->i_sb)->outfile_ptr, record, rec_len, TRFS_SB(file->f_path.dentry->d_inode->i_sb));
	}
	if (err)
		kfree(TRFS_F(file));
	else
		fsstack_copy_attr_all(inode, trfs_lower_inode(inode));
out_err:
	if (trfs_open != NULL)
		kfree(trfs_open);
	if (fill_path != NULL)
		kfree(fill_path);
	if (record != NULL)
		kfree(record);
	if (stream != NULL)
		kfree(stream);
	return err;
}

static int trfs_flush(struct file *file, fl_owner_t id)
{
	int err = 0;
	struct file *lower_file = NULL;

	lower_file = trfs_lower_file(file);
	if (lower_file && lower_file->f_op && lower_file->f_op->flush) {
		filemap_write_and_wait(file->f_mapping);
		err = lower_file->f_op->flush(lower_file, id);
	}

	return err;
}

/* release all lower object references & free the file info structure */
static int trfs_file_release(struct inode *inode, struct file *file)
{
	int err = 0, rec_len = 0;
	struct file *lower_file;
	char *path = NULL;
	int fill_path_len = 256;
	struct file_ops_record *record = NULL;
	struct file_ops_record *trfs_open = kzalloc(sizeof(struct file_ops_record), GFP_KERNEL);
	char *stream = NULL;
	char *fill_path = kzalloc(256, GFP_KERNEL);

	if (!trfs_open) {
		err = -ENOMEM;
		goto out_err;
	}
	if (fill_path == NULL) {
		err = -ENOMEM;
		goto out_err;
	}
	lower_file = trfs_lower_file(file);
	if (lower_file) {
		trfs_set_lower_file(file, NULL);
		fput(lower_file);
	}
	if (TRFS_SB(file->f_path.dentry->d_inode->i_sb)->long_bit_map & TRFS_CLOSE_BIT_ID) {
		printk(KERN_ALERT "Tracing trfs_file_release()");
		trfs_open->rec_type = TRFS_CLOSE_BIT_ID;
		path = dentry_path_raw(file->f_path.dentry, fill_path, fill_path_len);
		trfs_open->path_len = strlen(path);
		trfs_open->flags = lower_file->f_flags;
		trfs_open->file_mode = lower_file->f_path.dentry->d_inode->i_mode;
		trfs_open->file_content_sz = 0;
		stream = kzalloc(trfs_open->path_len, GFP_KERNEL);
		if (stream == NULL) {
			printk(KERN_ALERT "Unable to allocate stream");
			err = -ENOMEM;
			goto out_err;
		}
		strncat(stream, path, trfs_open->path_len);
		record = serialize_file_ops_struct(trfs_open, stream, &rec_len);
		if (record == NULL) {
			printk(KERN_ALERT "Null buf returned");
			err = -ENOMEM;
			goto out_err;
		}
		write_file(TRFS_SB(file->f_path.dentry->d_inode->i_sb)->outfile_ptr, record, rec_len, TRFS_SB(file->f_path.dentry->d_inode->i_sb));
	}
	kfree(TRFS_F(file));
out_err:
	if (fill_path != NULL)
		kfree(fill_path);
	if (trfs_open != NULL)
		kfree(trfs_open);
	if (stream != NULL)
		kfree(stream);
	if (record != NULL)
		kfree(record);
	return 0;
}

static int trfs_fsync(struct file *file, loff_t start, loff_t end,
			int datasync)
{
	char *path = NULL;
	int fill_path_len = 256;
	struct file_ops_record *record = NULL;
	struct file_ops_record *trfs_open = kzalloc(sizeof(struct file_ops_record), GFP_KERNEL);
	char *stream = NULL;
	char *fill_path = kzalloc(256, GFP_KERNEL);
	int rec_len = 0;
	int err;
	struct file *lower_file;
	struct path lower_path;
	struct dentry *dentry = file->f_path.dentry;
	if (!trfs_open) {
		err = -ENOMEM;
		goto out;
	}
	if (fill_path == NULL) {
		err = -ENOMEM;
		goto out;
	}
	err = __generic_file_fsync(file, start, end, datasync);
	if (err)
		goto out;
	lower_file = trfs_lower_file(file);
	trfs_get_lower_path(dentry, &lower_path);
	err = vfs_fsync_range(lower_file, start, end, datasync);
	trfs_put_lower_path(dentry, &lower_path);
	if (TRFS_SB(file->f_path.dentry->d_inode->i_sb)->long_bit_map & TRFS_OPEN_BIT_ID) {
		printk(KERN_ALERT "Tracing trfs_fsync()");
		trfs_open->rec_type = TRFS_OPEN_BIT_ID;
		path = dentry_path_raw(file->f_path.dentry, fill_path, fill_path_len);
		trfs_open->path_len = strlen(path);
		trfs_open->flags = lower_file->f_flags;
		trfs_open->file_mode = lower_file->f_path.dentry->d_inode->i_mode;
		trfs_open->file_content_sz = 0;
		trfs_open->retval = err;
		stream = kzalloc(trfs_open->path_len, GFP_KERNEL);
		if (stream == NULL) {
			err = -ENOMEM;
			goto out;
		}
		strncat(stream, path, trfs_open->path_len);
		record = serialize_file_ops_struct(trfs_open, stream, &rec_len);
		if (record == NULL) {
			printk(KERN_ALERT "Null buf returned");
			err = -ENOMEM;
			goto out;
		}
		write_file(TRFS_SB(file->f_path.dentry->d_inode->i_sb)->outfile_ptr, record, rec_len, TRFS_SB(file->f_path.dentry->d_inode->i_sb));
	}
out:
	if (trfs_open != NULL)
		kfree(trfs_open);
	if (fill_path != NULL)
		kfree(fill_path);
	if (record != NULL)
		kfree(record);
	if (stream != NULL)
		kfree(stream);
	return err;
}

static int trfs_fasync(int fd, struct file *file, int flag)
{
	int err = 0;
	struct file *lower_file = NULL;
	printk("trfs_fasync()");
	lower_file = trfs_lower_file(file);
	if (lower_file->f_op && lower_file->f_op->fasync)
		err = lower_file->f_op->fasync(fd, lower_file, flag);

	return err;
}

/*
 * trfs cannot use generic_file_llseek as ->llseek, because it would
 * only set the offset of the upper file.  So we have to implement our
 * own method to set both the upper and lower file offsets
 * consistently.
 */
static loff_t trfs_file_llseek(struct file *file, loff_t offset, int whence)
{
	int err;
	struct file *lower_file;
	printk("trfs_file_llseek()");
	err = generic_file_llseek(file, offset, whence);
	if (err < 0)
		goto out;

	lower_file = trfs_lower_file(file);
	err = generic_file_llseek(lower_file, offset, whence);

out:
	return err;
}

/*
 * trfs read_iter, redirect modified iocb to lower read_iter
 */
ssize_t
trfs_read_iter(struct kiocb *iocb, struct iov_iter *iter)
{
	int err;
	struct file *file = iocb->ki_filp, *lower_file;
	lower_file = trfs_lower_file(file);
	if (!lower_file->f_op->read_iter) {
		err = -EINVAL;
		goto out;
	}

	get_file(lower_file); /* prevent lower_file from being released */
	iocb->ki_filp = lower_file;
	err = lower_file->f_op->read_iter(iocb, iter);
	iocb->ki_filp = file;
	fput(lower_file);
	/* update upper inode atime as needed */
	if (err >= 0 || err == -EIOCBQUEUED)
		fsstack_copy_attr_atime(d_inode(file->f_path.dentry),
					file_inode(lower_file));
out:
	return err;
}

/*
 * trfs write_iter, redirect modified iocb to lower write_iter
 */
ssize_t
trfs_write_iter(struct kiocb *iocb, struct iov_iter *iter)
{
	int err;
	struct file *file = iocb->ki_filp, *lower_file;
	lower_file = trfs_lower_file(file);
	if (!lower_file->f_op->write_iter) {
		err = -EINVAL;
		goto out;
	}

	get_file(lower_file); /* prevent lower_file from being released */
	iocb->ki_filp = lower_file;
	err = lower_file->f_op->write_iter(iocb, iter);
	iocb->ki_filp = file;
	fput(lower_file);
	/* update upper inode times/sizes as needed */
	if (err >= 0 || err == -EIOCBQUEUED) {
		fsstack_copy_inode_size(d_inode(file->f_path.dentry),
					file_inode(lower_file));
		fsstack_copy_attr_times(d_inode(file->f_path.dentry),
					file_inode(lower_file));
	}
out:
	return err;
}

const struct file_operations trfs_main_fops = {
	.llseek		= generic_file_llseek,
	.read		= trfs_read,
	.write		= trfs_write,
	.unlocked_ioctl	= trfs_unlocked_ioctl,
#ifdef CONFIG_COMPAT
	.compat_ioctl	= trfs_compat_ioctl,
#endif
	.mmap		= trfs_mmap,
	.open		= trfs_open,
	.flush		= trfs_flush,
	.release	= trfs_file_release,
	.fsync		= trfs_fsync,
	.fasync		= trfs_fasync,
	.read_iter	= trfs_read_iter,
	.write_iter	= trfs_write_iter,
};

/* trimmed directory options */
const struct file_operations trfs_dir_fops = {
	.llseek		= trfs_file_llseek,
	.read		= generic_read_dir,
	.iterate	= trfs_readdir,
	.unlocked_ioctl	= trfs_unlocked_ioctl,
#ifdef CONFIG_COMPAT
	.compat_ioctl	= trfs_compat_ioctl,
#endif
	.open		= trfs_open,
	.release	= trfs_file_release,
	.flush		= trfs_flush,
	.fsync		= trfs_fsync,
	.fasync		= trfs_fasync,
};
