/*
 * Copyright (c) 1998-2015 Erez Zadok
 * Copyright (c) 2009	   Shrikar Archak
 * Copyright (c) 2003-2015 Stony Brook University
 * Copyright (c) 2003-2015 The Research Foundation of SUNY
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include "../../hw2/treplay.h"
#include "../../hw2/trctl.h"

#ifndef _TRFS_H_
#define _TRFS_H_

#include <linux/dcache.h>
#include <linux/file.h>
#include <linux/fs.h>
#include <linux/mm.h>
#include <linux/mount.h>
#include <linux/namei.h>
#include <linux/seq_file.h>
#include <linux/statfs.h>
#include <linux/fs_stack.h>
#include <linux/magic.h>
#include <linux/uaccess.h>
#include <linux/slab.h>
#include <linux/sched.h>
#include <linux/xattr.h>
#include <linux/exportfs.h>

/* the file system name */
#define TRFS_NAME "trfs"

/* trfs root inode number */
#define TRFS_ROOT_INO     1

/* useful for tracking code reachability */
#define UDBG printk(KERN_DEFAULT "DBG:%s:%s:%d\n", __FILE__, __func__, __LINE__)

#define CHECK_BIT(bit_map, index) ((bit_map) & (1<<(index)))

/* operations vectors defined in specific files */
extern const struct file_operations trfs_main_fops;
extern const struct file_operations trfs_dir_fops;
extern const struct inode_operations trfs_main_iops;
extern const struct inode_operations trfs_dir_iops;
extern const struct inode_operations trfs_symlink_iops;
extern const struct super_operations trfs_sops;
extern const struct dentry_operations trfs_dops;
extern const struct address_space_operations trfs_aops, trfs_dummy_aops;
extern const struct vm_operations_struct trfs_vm_ops;
extern const struct export_operations trfs_export_ops;

extern int trfs_init_inode_cache(void);
extern void trfs_destroy_inode_cache(void);
extern int trfs_init_dentry_cache(void);
extern void trfs_destroy_dentry_cache(void);
extern int new_dentry_private_data(struct dentry *dentry);
extern void free_dentry_private_data(struct dentry *dentry);
extern struct dentry *trfs_lookup(struct inode *dir, struct dentry *dentry,
				    unsigned int flags);
extern struct inode *trfs_iget(struct super_block *sb,
				 struct inode *lower_inode);
extern int trfs_interpose(struct dentry *dentry, struct super_block *sb,
			    struct path *lower_path);

/* file private data */
struct trfs_file_info {
	struct file *lower_file;
	const struct vm_operations_struct *lower_vm_ops;
};

/*trfs mount parameters info */
struct trfs_mount_info {
	char *device_name;
	char *output_path;
};

/* trfs inode data in memory */
struct trfs_inode_info {
	struct inode *lower_inode;
	struct inode vfs_inode;
};


/* trfs dentry data in memory */
struct trfs_dentry_info {
	spinlock_t lock;	/* protects lower_path */
	struct path lower_path;
};

/* trfs super-block data in memory */
struct trfs_sb_info {
	struct super_block *lower_sb;
	struct file *outfile_ptr;
	char *bit_map;
	spinlock_t rec_lock;
	unsigned long long_bit_map;
	unsigned int rec_id;
};

/*
 * inode to private data
 *
 * Since we use containers and the struct inode is _inside_ the
 * trfs_inode_info structure, TRFS_I will always (given a non-NULL
 * inode pointer), return a valid non-NULL pointer.
 */
static inline struct trfs_inode_info *TRFS_I(const struct inode *inode)
{
	return container_of(inode, struct trfs_inode_info, vfs_inode);
}

/* dentry to private data */
#define TRFS_D(dent) ((struct trfs_dentry_info *)(dent)->d_fsdata)

/* superblock to private data */
#define TRFS_SB(super) ((struct trfs_sb_info *)(super)->s_fs_info)

/* file to private Data */
#define TRFS_F(file) ((struct trfs_file_info *)((file)->private_data))

/* file to lower file */
static inline struct file *trfs_lower_file(const struct file *f)
{
	return TRFS_F(f)->lower_file;
}

static inline void trfs_set_lower_file(struct file *f, struct file *val)
{
	TRFS_F(f)->lower_file = val;
}

/* inode to lower inode. */
static inline struct inode *trfs_lower_inode(const struct inode *i)
{
	return TRFS_I(i)->lower_inode;
}

static inline void trfs_set_lower_inode(struct inode *i, struct inode *val)
{
	TRFS_I(i)->lower_inode = val;
}

/* superblock to lower superblock */
static inline struct super_block *trfs_lower_super(
	const struct super_block *sb)
{
	return TRFS_SB(sb)->lower_sb;
}

static inline void trfs_set_lower_super(struct super_block *sb,
					  struct super_block *val, struct file *outfile_ptr,
					   char *bit_map, unsigned long long_bit_map,
					    unsigned int rec_id)
{
	TRFS_SB(sb)->lower_sb = val;
	TRFS_SB(sb)->outfile_ptr = outfile_ptr;
	TRFS_SB(sb)->bit_map = bit_map;
	TRFS_SB(sb)->long_bit_map = long_bit_map;
	TRFS_SB(sb)->rec_id = rec_id;
	spin_lock_init(&TRFS_SB(sb)->rec_lock);
}

/* path based (dentry/mnt) macros */
static inline void pathcpy(struct path *dst, const struct path *src)
{
	dst->dentry = src->dentry;
	dst->mnt = src->mnt;
}
/* Returns struct path.  Caller must path_put it. */
static inline void trfs_get_lower_path(const struct dentry *dent,
					 struct path *lower_path)
{
	spin_lock(&TRFS_D(dent)->lock);
	pathcpy(lower_path, &TRFS_D(dent)->lower_path);
	path_get(lower_path);
	spin_unlock(&TRFS_D(dent)->lock);
	return;
}
static inline void trfs_put_lower_path(const struct dentry *dent,
					 struct path *lower_path)
{
	path_put(lower_path);
	return;
}
static inline void trfs_set_lower_path(const struct dentry *dent,
					 struct path *lower_path)
{
	spin_lock(&TRFS_D(dent)->lock);
	pathcpy(&TRFS_D(dent)->lower_path, lower_path);
	spin_unlock(&TRFS_D(dent)->lock);
	return;
}
static inline void trfs_reset_lower_path(const struct dentry *dent)
{
	spin_lock(&TRFS_D(dent)->lock);
	TRFS_D(dent)->lower_path.dentry = NULL;
	TRFS_D(dent)->lower_path.mnt = NULL;
	spin_unlock(&TRFS_D(dent)->lock);
	return;
}
static inline void trfs_put_reset_lower_path(const struct dentry *dent)
{
	struct path lower_path;

	spin_lock(&TRFS_D(dent)->lock);
	pathcpy(&lower_path, &TRFS_D(dent)->lower_path);
	TRFS_D(dent)->lower_path.dentry = NULL;
	TRFS_D(dent)->lower_path.mnt = NULL;
	spin_unlock(&TRFS_D(dent)->lock);
	path_put(&lower_path);
	return;
}

/* locking helpers */
static inline struct dentry *lock_parent(struct dentry *dentry)
{
	struct dentry *dir = dget_parent(dentry);

	inode_lock_nested(d_inode(dir), I_MUTEX_PARENT);
	return dir;
}

static inline void unlock_dir(struct dentry *dir)
{
	inode_unlock(d_inode(dir));
	dput(dir);
}

static inline int set_long_bit_map(struct file *file, char *bit_map)
{
	unsigned long long_bit_map = 0;
	char *iter = bit_map + 2;
	bool invalid_string = false;

	while (*iter != '\0') {
		if (*iter >= '0' && *iter <= '9')
			long_bit_map = (long_bit_map << 4) + (*iter - '0');
		else if (*iter >= 'A' && *iter <= 'F')
			long_bit_map = (long_bit_map << 4) + (*iter - 'A' + 10);
		else if (*iter >= 'a' && *iter <= 'f')
			long_bit_map = (long_bit_map << 4) + (*iter - 'a' + 10);
		else {
			invalid_string = true;
			break;
		}
		iter++;
	}
	if (invalid_string) {
		printk(KERN_ALERT "set_long_bit_map() failed.");
		return -1;
	}
	TRFS_SB((file->f_path.dentry->d_inode->i_sb))->long_bit_map =
	 long_bit_map;
	return 0;
}

static inline struct file *open_outfile(char *outfile, umode_t mode)
{
	struct file *file_ptr;
	file_ptr = filp_open(outfile, O_WRONLY|O_CREAT|O_APPEND, mode);
	if (file_ptr == NULL || IS_ERR(file_ptr))
		printk(KERN_ALERT "Unable to open file in write mode!");
	return file_ptr;
}

static inline struct file_ops_record *serialize_file_ops_struct(
	struct file_ops_record *file_ops, char *stream, int *len)
{
	int size_of_buffer = sizeof(struct file_ops_record) + strlen(stream);
	struct file_ops_record *expanded_file_ops = NULL;
	*len = size_of_buffer;
	file_ops->rec_sz = size_of_buffer;
	file_ops->stream_len = strlen(stream);
	expanded_file_ops = kzalloc(size_of_buffer, GFP_KERNEL);
	if (expanded_file_ops == NULL)
		return NULL;
	memcpy(expanded_file_ops, file_ops, sizeof(struct file_ops_record));
	memcpy(&expanded_file_ops->stream, stream, strlen(stream));
	printk(KERN_ALERT "******************\n");
	printk(KERN_ALERT "->rec_sz: %d\n", expanded_file_ops->rec_sz);
	printk(KERN_ALERT "->rec_type: %d\n", expanded_file_ops->rec_type);
	printk(KERN_ALERT "->flags: %d\n", expanded_file_ops->flags);
	printk(KERN_ALERT "->file_mode: %d\n", expanded_file_ops->file_mode);
	printk(KERN_ALERT "->path_len: %d\n", expanded_file_ops->path_len);
	printk(KERN_ALERT "->file_csz: %d\n", expanded_file_ops->file_content_sz);
	printk(KERN_ALERT "->offset: %lld\n", expanded_file_ops->offset);
	printk(KERN_ALERT "->stream_len: %d\n", expanded_file_ops->stream_len);
	printk(KERN_ALERT "->retval: %d\n", expanded_file_ops->retval);
	printk(KERN_ALERT "->stream: %s\n", expanded_file_ops->stream);
	printk(KERN_ALERT "******************\n");
	return expanded_file_ops;
}

static inline int write_file(struct file *file_ptr, void *buffer,
	int size, struct trfs_sb_info *sb)
{
	mm_segment_t old_fs;
	int bytes = 0;
	/* -- start lock -- */
	spin_lock(&sb->rec_lock);
	sb->rec_id = sb->rec_id + 1;
	memcpy(buffer + sizeof(unsigned int), &(sb->rec_id),
	 sizeof(unsigned int));
	old_fs = get_fs();
	set_fs(KERNEL_DS);
	bytes = vfs_write(file_ptr, buffer, size, &file_ptr->f_pos);
	set_fs(old_fs);
	spin_unlock(&sb->rec_lock);
	/* -- end lock -- */
	return bytes;
}
#endif	/* not _TRFS_H_ */
